from solution import a_plus_b


def test():
    assert a_plus_b(0, 0) == 0
    assert a_plus_b(1, 2) == 3
    assert a_plus_b(10, 2) == 12
    assert a_plus_b(0, 2) == 2
    assert a_plus_b(2, 1) == 3
    assert a_plus_b(-100, 10) == -90


if __name__ == '__main__':
    test()